#!/usr/bin/env python

import numpy
import math
from os import listdir

from os import listdir

#listdir(".") write files name from current working directory in list 
	


# load db from files are in current working directory
source_file = "source/0924.txt"

db = numpy.loadtxt(source_file,dtype = numpy.float, delimiter=",")
X	 	= db[:,0]
Y	 	= db[:,1]
Z	 	= db[:,2]
area	= db[:,3]
den	 	= db[:,4]
temp	= db[:,5]
v		= db[:,6]


#initialisation coefficients boundary lines for i,j slits? where i - number sectors, j - number slit

aMIN = [[0,0,0,0,0],
	 	[-1.722727928,-1.724192662,-1.724806751,-1.725284406,-1.72666154],
	 	[1.722727928,1.724192662,1.724806751,1.725284406, 1.72666154],
	 	[0,0,0,0,0],
	 	[-1.722727928,-1.724192662,-1.724806751,-1.725284406,-1.72666154],
	 	[1.722727928,1.724192662,1.724806751,1.725284406,1.72666154]]


bMIN = [[0.036292893,0.041892893,0.047892893,0.053892893,0.059892893],
	 	[0.071872209,0.083088844,0.095071348,0.107053897,0.119087846],
	 	[-0.071872209,-0.083088844,-0.095071348,-0.107053897,-0.119087846],
	 	[-0.036292893,-0.041892893,-0.047892893,-0.053892893,-0.059892893],
	 	[-0.071872209,-0.083088844,-0.095071348,-0.107053897,-0.119087846],
	 	[0.071872209,0.083088844,0.095071348,0.107053897,0.119087846]]

aMAX = [[0,0,0,0,0],
	 	[-1.740702668,-1.739854015,-1.738626163,-1.7376501,-1.737082039],
	 	[1.740702668,1.739854015,1.738626163,1.7376501,1.737082039],
	 	[0,0,0,0,0],
	 	[-1.740702668,-1.739854015,-1.738626163,-1.7376501,-1.737082039],
	 	[1.740702668,1.739854015,1.738626163,1.7376501,1.737082039]]

bMAX = [[0.040807107,0.046807107,0.052807107, 0.058807107,0.064607107],
	 	[0.082335236,0.094352283,0.106334376,0.118316595,0.129916366],
	 	[-0.082335236,-0.094352283,-0.106334376,-0.118316595,-0.129916366],
	 	[-0.040807107,-0.046807107,-0.052807107,-0.058807107,-0.064607107],
	 	[-0.082335236,-0.094352283,-0.106334376,-0.118316595,-0.129916366],
	 	[0.082335236,0.094352283,0.106334376,0.118316595,0.129916366]]



print ("All point in cross section: " + str(len(Y)))

#create restricting lines
# for each slit exist four lines
# two lines leaving from center coord - denoted l1 and l2
# two lines restricting slit up and down - lmax and lmin
# each line depends of coord X => for each line create functions
# in functions name specify number sectors and slit


# --------------------- Function template -----------------
# first index sector number second index slit number
def tan60(X):
	argpi = math.pi / 3 
	return math.tan(argpi) * X 
def tan120(X):
	argpi = 2 * math.pi / 3 
	return math.tan(argpi) * X
def tan180(X):
	argpi = math.pi 
	return math.tan(argpi) * X 
def LMIN(X, aMIN, bMIN):
	#ymin = a * X + b
	return aMIN * X + bMIN
def LMAX(X, aMAX, bMAX):
	#ymax = a * X + b
	return aMAX * X + bMAX
#---------------------------------------------------------

#----------------------Logic function------------------------
def selectS1(X, aMIN, bMIN, aMAX, bMAX, i):
	if ( Y[i] > tan60(X[i]) and 
		 Y[i] > tan120(X[i]) and 
 		 Y[i] > LMIN(X[i], aMIN, bMIN) and 
 		 Y[i] < LMAX(X[i], aMAX, bMAX)):
		return 1
	else:
		return 0
def selectS2(X, aMIN, bMIN, aMAX, bMAX, i):
	if ( Y[i] < tan60(X[i]) and 
		 Y[i] > tan180(X[i]) and 
 		 Y[i] > LMIN(X[i], aMIN, bMIN) and 
 		 Y[i] < LMAX(X[i], aMAX, bMAX)):
		return 1
	else:
		return 0
def selectS3(X, aMIN, bMIN, aMAX, bMAX, i):
	if ( Y[i] < tan180(X[i]) and 
		 Y[i] > tan120(X[i]) and 
 		 Y[i] < LMIN(X[i], aMIN, bMIN) and 
 		 Y[i] > LMAX(X[i], aMAX, bMAX)):
		return 1
	else:
		return 0
def selectS4(X, aMIN, bMIN, aMAX, bMAX, i):
	if ( Y[i] < tan120(X[i]) and 
		 Y[i] < tan60(X[i]) and 
 		 Y[i] < LMIN(X[i], aMIN, bMIN) and 
 		 Y[i] > LMAX(X[i], aMAX, bMAX)):
		return 1
	else:
		return 0
def selectS5(X, aMIN, bMIN, aMAX, bMAX, i):
	if ( Y[i] > tan60(X[i]) and 
		 Y[i] < tan180(X[i]) and 
 		 Y[i] < LMIN(X[i], aMIN, bMIN) and 
 		 Y[i] > LMAX(X[i], aMAX, bMAX)):
		return 1
	else:
		return 0
def selectS6(X, aMIN, bMIN, aMAX, bMAX, i):
	if ( Y[i] > tan180(X[i]) and 
		 Y[i] < tan120(X[i]) and 
 		 Y[i] > LMIN(X[i], aMIN, bMIN) and 
 		 Y[i] < LMAX(X[i], aMAX, bMAX)):
		return 1
	else:
		return 0
#---------------------------------------------------------

#----------------Function for solution of phisical value----------------------

def SolutMR(X, aMIN, bMIN, aMAX, bMAX, s, sl):
	# mrSec1Slit1 - Mass Rate into slit 1 and sector 1
	mr = []
	for i in range(len(Y)):
		if s == 0:
			#selectS1
			if(selectS1(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = den[i] * v[i] * area[i]
				mr.append(tmp)
		elif s == 1:
			#selectS2
			if(selectS2(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = den[i] * v[i] * area[i]
				mr.append(tmp)
		elif s == 2:
			#selectS3
			if(selectS3(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = den[i] * v[i] * area[i]
				mr.append(tmp)
		elif s == 3:
			#selectS4
			if(selectS4(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = den[i] * v[i] * area[i]
				mr.append(tmp)	
		elif s == 4:
			#selectS5
			if(selectS5(X, aMIN, bMIN, aMAX, bMAX, i) == 1):

				tmp = den[i] * v[i] * area[i]
				mr.append(tmp)
	
		elif s == 5:
			#selectS6
			if(selectS6(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = den[i] * v[i] * area[i]
				mr.append(tmp)	
		else: 
			print ("Error in number sectors")
	
	print len(mr) #count point selecting in slit (i,j)
	#sum(mr)wrong result
	tmp = 0
	for j in range(len(mr)):
		tmp = tmp + mr[j]
	return tmp
	#print ("MassRate " + str(s + 1) + " sectors and " + str(sl + 1) + " slits: " + str(tmp))		


#----------------------------------------------------------------------------------------------
def SolutAT(X, aMIN, bMIN, aMAX, bMAX, s, sl):
	# mrSec1Slit1 - Mass Rate into slit 1 and sector 1
	at = []#list for average temperature
	for i in range(len(Y)):
		if s == 0:
			#selectS1
			if(selectS1(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = (den[i] * v[i] * area[i] * temp[i]) 
				at.append(tmp)
		elif s == 1:
			#selectS2
			if(selectS2(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = (den[i] * v[i] * area[i] * temp[i]) 
				at.append(tmp)
		elif s == 2:
			#selectS3
			if(selectS3(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = (den[i] * v[i] * area[i] * temp[i]) 
				at.append(tmp)	
		elif s == 3:
			#selectS4
			if(selectS4(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = (den[i] * v[i] * area[i] * temp[i]) 
				at.append(tmp)	
		elif s == 4:
			#selectS5
			if(selectS5(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = (den[i] * v[i] * area[i] * temp[i])
				at.append(tmp)	
		elif s == 5:
			#selectS6
			if(selectS6(X, aMIN, bMIN, aMAX, bMAX, i) == 1):
				tmp = (den[i] * v[i] * area[i] * temp[i])
				at.append(tmp)	
		else: 
			print ("Error in number sectors")
	
	print len(at) #count point selecting in slit (i,j)
	#sum(mr)wrong result
	ttmp = 0
	for j in range(len(at)):
		ttmp = ttmp + at[j]
	return ttmp / SolutMR(X, aMIN, bMIN, aMAX, bMAX, s, sl)
	#ttmp/sum(at)
	print ("Average temperature in " + str(s + 1) + " sectors and " + str(sl + 1) + " slits: " + str(ttmp))


def main():
	f = open('res/res.txt','w')
	f.write('Solution mass rate based from source file: ' + str(source_file) + '\n')
	for s in range(6):#cicle for sectors 0 1 2 3 4 5
		for sl in range(5):#cicle for slits 0 1 2 3 4
			mrShow = SolutMR(X, aMIN[s][sl], bMIN[s][sl], aMAX[s][sl], bMAX[s][sl],s,sl)
			print ("MassRate " + str(s + 1) + " sectors and " + str(sl + 1) + " slits: " + str(mrShow))
			f.write(str(mrShow) + '\t')
		f.write(str(mrShow) + '\n')

	f.write('\n'*2)

	f.write('Solution average temperature based from source file: ' + str(source_file) + '\n')
	for s in range(6):#cicle for sectors 0 1 2 3 4 5
		for sl in range(5):#cicle for slits 0 1 2 3 4
			atShow = SolutAT(X, aMIN[s][sl], bMIN[s][sl], aMAX[s][sl], bMAX[s][sl],s,sl) - 273
			print ("MassRate " + str(s + 1) + " sectors and " + str(sl + 1) + " slits: " + str(atShow))
			f.write(str(atShow) + '\t')
		f.write(str(atShow) + '\n')

	
if __name__ == "__main__":
	main()
